import React, { Component } from "react";
import axios from "axios";

class Form extends Component {
    constructor() {
        super()
        this.state = {
            concorrentes:[],
            concorrente: "",
            cnpjCpf: "",
            email: "",
            ramoAtividades:[],
            ramoAtividade: "",
            txDebitoConcorrente: "",
            descontoDebito: "",
            txCredConcorrente: "",
            descontoCredito: "",
            proposta: false
        };
        this.handleChange = this.handleChange.bind(this)
        this.handleSubmit = this.handleSubmit.bind(this)
    }


    getConcorrentes() {
        axios
            .get(`http://localhost:8080/concorrentes/all`)
            .then(response => {
                return this.setState({
                    concorrentes: response.data
                });
            })
            .catch(error => console.log(error));
    }

    getRamoAtividade() {
        axios
            .get(`http://localhost:8080/ramo-atividade/all`)
            .then(response => {
                return this.setState({
                    ramoAtividades: response.data
                });
            })
            .catch(error => console.log(error));
    }

    componentDidMount() {
        this.getConcorrentes();
        this.getRamoAtividade();
    }

    handleChange = (event) => {
        this.setState({
            [event.target.name]: event.target.value
        });
    };

    handleSubmit = event => {
        event.preventDefault();
        const clientData = {
            concorrente: this.state.concorrente,
            cnpjCpf: this.state.cnpjCpf,
            email: this.state.email,
            ramoAtividade: this.state.ramoAtividade,
            txDebitoConcorrente: this.state.txDebitoConcorrente,
            descontoDebito: this.state.descontoDebito,
            txCredConcorrente: this.state.txCredConcorrente,
            descontoCredito: this.state.descontoCredito,
            proposta: this.state.proposta
        };
        console.dir(clientData)
        axios
            .post(`http://localhost:8080/clientes`, clientData)
            .then(response => console.log(response.data))
    }

    render() {
        return (
            <div className="form">
                <div className="container">
                    <div className="row">
                        <div className="col-md-8 m-auto">
                            <h4 className="display-4 text-center">Simulação</h4>
                            <form onSubmit={this.handleSubmit}>
                                <div className="form-group">
                                    <select
                                        className="form-control form-control-lg"
                                        name="concorrente"
                                        value={this.state.concorrente}
                                        onChange={this.handleChange}
                                    >
                                        <option value="">Selecione Concorrente</option>
                                        {this.state.concorrentes.map((item, key) => (
                                            <option value={item.id} key={key}>
                                                {item.nome}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        name="cnpjCpf"
                                        value={this.state.cnpjCpf}
                                        placeholder="CNPJ/CPF"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="text"
                                        className="form-control form-control-lg"
                                        name="email"
                                        value={this.state.email}
                                        placeholder="email@example.com"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <select
                                        className="form-control form-control-lg"
                                        name="ramoAtividade"
                                        value={this.state.ramoAtividade}
                                        onChange={this.handleChange}
                                    >
                                        <option value="">Selecione Ramo de Atividade</option>
                                        {this.state.ramoAtividades.map((item, key) => (
                                            <option value={item.id} key={key}>
                                                {item.nome}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <div className="form-group">
                                    <input
                                        type="number"
                                        className="form-control form-control-lg"
                                        name="txDebitoConcorrente"
                                        value={this.state.txDebitoConcorrente}
                                        placeholder="Taxa Débito Concorrente"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="number"
                                        className="form-control form-control-lg"
                                        name="descontoDebito"
                                        value={this.state.descontoDebito}
                                        placeholder="Desconto Débito"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="number"
                                        className="form-control form-control-lg"
                                        name="txCredConcorrente"
                                        value={this.state.txCredConcorrente}
                                        placeholder="Taxa Crédito Concorrente"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-group">
                                    <input
                                        type="number"
                                        className="form-control form-control-lg"
                                        name="descontoCredito"
                                        value={this.state.descontoCredito}
                                        placeholder="Desconto Crédito"
                                        onChange={this.handleChange}
                                    />
                                </div>
                                <div className="form-check form-check-inline">
                                    <input
                                        className="form-check-input"
                                        type="radio"
                                        name="proposta"
                                        value={this.state.proposta}
                                        onChange={this.handleChange}
                                    />
                                    <label className="form-check-label">Proposta aceita</label>
                                </div>
                                <input
                                    type="submit"
                                    className="btn btn-primary btn-block mt-4"
                                />
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        );
    }
}

export default Form;
