import React, { Component } from 'react';
import "bootstrap/dist/css/bootstrap.min.css";
import Form from './components/form/Form';
import Header from './components/header/Header';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header />      
        <Form />
      </div>
        
      
    );
  }
}

export default App;
